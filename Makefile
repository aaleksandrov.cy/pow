.PHONY: run_server run_client

run_server:
	go run ./cmd/server/main.go

run_client:
	go run ./cmd/client/main.go

lint:
	golangci-lint run ./...