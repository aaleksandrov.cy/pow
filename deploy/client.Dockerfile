FROM golang:alpine as builder
RUN apk add ca-certificates git gcc musl-dev mingw-w64-gcc
WORKDIR /opt
COPY go.mod ./
COPY cmd/client cmd/client
COPY internal   internal
RUN cd /opt/cmd/client && \
    go build -o /srv/client

FROM alpine:latest
COPY --from=builder /srv /srv
WORKDIR /srv
CMD /srv/client
