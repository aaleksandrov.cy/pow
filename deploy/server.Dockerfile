FROM golang:alpine as builder
RUN apk add ca-certificates git gcc musl-dev mingw-w64-gcc
WORKDIR /opt
COPY go.mod ./
COPY cmd/server  cmd/server
COPY internal    internal
RUN cd /opt/cmd/server && \
    go build -o /srv/server

FROM alpine:latest
COPY --from=builder /srv /srv
WORKDIR /srv
CMD /srv/server
