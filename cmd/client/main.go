package main

import (
	"gitlab.com/aaleksandrov.cy/pow/internal/conf"
	"gitlab.com/aaleksandrov.cy/pow/internal/pow"
	"io"
	"log"
	"net"
)

func main() {
	address := conf.GetEnv("SERVER_ADDRESS", "127.0.0.1:3333")

	conn, err := net.Dial("tcp", address)
	if err != nil {
		log.Fatal(err)
	}

	if err = pow.Connect(conn); err != nil {
		log.Fatal(err)
	}

	quote, err := io.ReadAll(conn)
	if err != nil {
		log.Fatal(err)
	}

	log.Println(string(quote))

}
