package main

import (
	"gitlab.com/aaleksandrov.cy/pow/internal/conf"
	"gitlab.com/aaleksandrov.cy/pow/internal/quotes"
	"gitlab.com/aaleksandrov.cy/pow/internal/server"
	"log"
	"os"
	"os/signal"
)

func main() {
	address := conf.GetEnv("LISTEN_ADDRESS", ":3333")
	quotesService := quotes.NewCollection()
	s := server.New(address, quotesService)
	if err := s.Start(); err != nil {
		log.Fatal(err)
	}
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	_ = s.Stop()
}
