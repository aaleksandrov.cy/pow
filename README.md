# pow

## Task description

Design and implement “Word of Wisdom” tcp server.  
• TCP server should be protected from DDOS attacks with the Prof of Work (https://en.wikipedia.org/wiki/Proof_of_work), the challenge-response protocol should be used.  
• The choice of the POW algorithm should be explained.  
• After Prof Of Work verification, server should send one of the quotes from “word of wisdom” book or any other collection of the quotes.  
• Docker file should be provided both for the server and for the client that solves the POW challenge  

## Running

Requires docker to run

```shell
docker-compose up
```

## PoW algorithm

This project implements an approach inspired by the hashcash implementation using the sha256 hashing algorithm. The idea is that when connecting to the server, the client receives a puzzle consisting of an arbitrary string of 64 bytes and the number of zeros that must be in the hash prefix. The number of zeros is determined when creating the test, but the current implementation still uses a static complexity of 5 zeros.  

The choice of this algorithm is due to:
- ease of implementation of the algorithm;
- the use of built-in hash libraries (sha256);
- fast verification of the solution on the server.
