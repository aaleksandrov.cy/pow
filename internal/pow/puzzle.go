package pow

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"math"
	"strings"
)

type Complexity uint8

const (
	ComplexityEasy Complexity = iota
	ComplexityNormal
	ComplexityHard

	puzzleDataSize = 64
)

type puzzle struct {
	data      []byte
	zeroCount uint8
}

func newPuzzle(complexity Complexity) (*puzzle, error) {
	var err error

	p := &puzzle{
		data:      make([]byte, puzzleDataSize),
		zeroCount: complexityToZeroCount(complexity),
	}
	_, err = rand.Read(p.data)

	if err != nil {
		return nil, err
	}
	return p, nil
}

func complexityToZeroCount(c Complexity) uint8 {
	var zeroCount uint8 = 5
	switch c {
	case ComplexityEasy:
		zeroCount = 4
	case ComplexityNormal:
		zeroCount = 5
	case ComplexityHard:
		zeroCount = 6
	}
	return zeroCount
}

func puzzleFromBytes(b []byte) *puzzle {
	return &puzzle{
		data:      b[:len(b)-1],
		zeroCount: b[len(b)-1],
	}
}

func (p *puzzle) bytes() []byte {
	buf := make([]byte, puzzleDataSize+1) // +1 byte for zeroCount
	copy(buf, p.data)
	buf[64] = p.zeroCount
	return buf
}

func (p *puzzle) isValidSolution(solution []byte) bool {
	hash := sha256.Sum256(append(p.data, solution...))
	return isValidHash(hash, p.zeroCount)
}

func (p *puzzle) solve() ([]byte, error) {
	var counter uint32 = 0
	for {
		nonce := make([]byte, solutionSize)
		binary.BigEndian.PutUint32(nonce, counter)
		hash := sha256.Sum256(append(p.data, nonce...))
		if isValidHash(hash, p.zeroCount) {
			return nonce, nil
		}
		counter++
		if counter == math.MaxUint32 {
			return nil, ErrNonceFindFail
		}
	}
}

func isValidHash(hash [32]byte, zeroCount uint8) bool {
	return strings.HasPrefix(hex.EncodeToString(hash[:]), strings.Repeat("0", int(zeroCount)))
}
