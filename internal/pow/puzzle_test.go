package pow

import (
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"strconv"
	"testing"
)

type TestCase struct {
	p                puzzle
	expectedSolution uint32
	expectedHash     string
}

func TestPuzzle(t *testing.T) {

	testCases := []*TestCase{
		{
			p: puzzle{
				data:      []byte("test"),
				zeroCount: complexityToZeroCount(ComplexityEasy),
			},
			expectedSolution: 16081,
			expectedHash:     "0000160a667f0e3ceae1f6ac33159e3d9c6ed47e71f014632a439f0b946b4110",
		},
		{
			p: puzzle{
				data:      []byte("test"),
				zeroCount: complexityToZeroCount(ComplexityNormal),
			},
			expectedSolution: 377950,
			expectedHash:     "00000ac179744298d95838870f1bbba6c49c886bc8b24e37fe56df2b965757e2",
		},
		{
			p: puzzle{
				data:      []byte("test"),
				zeroCount: complexityToZeroCount(ComplexityHard),
			},
			expectedSolution: 1904087,
			expectedHash:     "0000007c764144bf9d31dce2f2d4649c2bc0c30e8e7ada2dc19150b748f23984",
		},
	}

	for i, tc := range testCases {
		t.Run("test_"+strconv.Itoa(i), func(t *testing.T) {
			solution, err := tc.p.solve()
			if err != nil {
				t.Error(err)
			}
			solutionNum := binary.BigEndian.Uint32(solution)
			if tc.expectedSolution != solutionNum {
				t.Errorf("expected solution %d, got %d", tc.expectedSolution, solutionNum)
			}

			if !tc.p.isValidSolution(solution) {
				t.Error("invalid solution")
			}

			hash := sha256.Sum256(append(tc.p.data, solution...))
			hashStr := hex.EncodeToString(hash[:])

			if tc.expectedHash != hashStr {
				t.Errorf("expected hash %s, got %s", tc.expectedHash, hashStr)
			}
		})
	}
}

func BenchmarkSolveEasy(b *testing.B) {
	p := puzzle{data: []byte("test"), zeroCount: complexityToZeroCount(ComplexityEasy)}
	for i := 0; i < b.N; i++ {
		_, _ = p.solve()
	}
}

func BenchmarkSolveNormal(b *testing.B) {
	p := puzzle{data: []byte("test"), zeroCount: complexityToZeroCount(ComplexityNormal)}
	for i := 0; i < b.N; i++ {
		_, _ = p.solve()
	}
}

func BenchmarkSolveHard(b *testing.B) {
	p := puzzle{data: []byte("test"), zeroCount: complexityToZeroCount(ComplexityHard)}
	for i := 0; i < b.N; i++ {
		_, _ = p.solve()
	}
}
