package pow

import (
	"errors"
	"net"
)

const (
	solutionSize = 4
)

var (
	ErrNonceFindFail   = errors.New("failed to find nonce")
	ErrInvalidSolution = errors.New("invalid solution")
)

func Connect(conn net.Conn) error {
	buf := make([]byte, puzzleDataSize+1)

	_, err := conn.Read(buf)
	if err != nil {
		return err
	}

	solution, err := puzzleFromBytes(buf).solve()
	if err != nil {
		return err
	}

	_, err = conn.Write(solution)
	if err != nil {
		return err
	}

	return nil
}

func StartChallenge(conn net.Conn, complexity Complexity) error {

	pzl, err := newPuzzle(complexity)
	if err != nil {
		return err
	}

	_, err = conn.Write(pzl.bytes())
	if err != nil {
		return err
	}

	solution := make([]byte, solutionSize)
	_, err = conn.Read(solution)
	if err != nil {
		return err
	}

	if !pzl.isValidSolution(solution) {
		return ErrInvalidSolution
	}

	return nil
}
