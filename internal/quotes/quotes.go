package quotes

import (
	"math/rand"
	"time"
)

type Collection struct {
	quotes []string
}

func NewCollection() *Collection {
	rand.Seed(time.Now().UnixNano())
	return &Collection{
		quotes: []string{
			"The only way to do great work is to love what you do. - Steve Jobs",
			"In the middle of every difficulty lies opportunity. - Albert Einstein",
			"Success is not final, failure is not fatal: It is the courage to continue that counts. - Winston Churchill",
			"The best way to predict the future is to create it. - Peter Drucker",
			"The only limit to our realization of tomorrow will be our doubts of today. - Franklin D. Roosevelt",
			"The greatest glory in living lies not in never falling, but in rising every time we fall. - Nelson Mandela",
			"The future belongs to those who believe in the beauty of their dreams. - Eleanor Roosevelt",
			"The only person you are destined to become is the person you decide to be. - Ralph Waldo Emerson",
			"Success usually comes to those who are too busy to be looking for it. - Henry David Thoreau",
			"Don't watch the clock; do what it does. Keep going. - Sam Levenson",
			"Believe you can and you're halfway there. - Theodore Roosevelt",
			"A person who never made a mistake never tried anything new. - Albert Einstein",
			"Your time is limited, don't waste it living someone else's life. - Steve Jobs",
			"The biggest risk is not taking any risk. In a world that's changing really quickly, the only strategy that is guaranteed to fail is not taking risks. - Mark Zuckerberg",
			"The harder I work, the luckier I get. - Samuel Goldwyn",
		},
	}
}

func (c *Collection) GetRandomQuote() string {
	return c.quotes[rand.Intn(len(c.quotes))]
}
