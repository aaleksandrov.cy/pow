package server

import (
	"errors"
	"gitlab.com/aaleksandrov.cy/pow/internal/pow"
	"gitlab.com/aaleksandrov.cy/pow/internal/quotes"
	"log"
	"net"
	"time"
)

var (
	ErrListenerIsNil          = errors.New("listener is nil")
	connectionTimeoutDuration = time.Second * 2
)

type Server struct {
	address    string
	listener   net.Listener
	collection *quotes.Collection
}

func New(address string, collection *quotes.Collection) *Server {
	return &Server{
		address:    address,
		collection: collection,
	}
}

func (s *Server) Start() error {
	var err error
	s.listener, err = net.Listen("tcp", s.address)
	if err != nil {
		return err
	}
	go s.listen()
	log.Printf("server started on %s", s.address)
	return nil
}

func (s *Server) Stop() error {
	if s.listener != nil {
		return ErrListenerIsNil
	}
	return s.listener.Close()
}

func (s *Server) listen() {
	if s.listener == nil {
		panic(ErrListenerIsNil)
	}
	for i := 1; ; i++ {
		conn, err := s.listener.Accept()
		if err != nil {
			log.Printf("Client %d: connect error: %v\n", i, err)
			continue
		}
		start := time.Now()
		_ = conn.SetDeadline(time.Now().Add(connectionTimeoutDuration))
		go func(clientID int) {

			defer conn.Close()

			if err = pow.StartChallenge(conn, pow.ComplexityNormal); err != nil {
				log.Printf("Client %d: error: %v\n", clientID, err)
				return
			}

			_, err = conn.Write([]byte(s.collection.GetRandomQuote()))
			if err != nil {
				log.Println(err)
				return
			}

			log.Printf("Client %d: elapsed %v", clientID, time.Since(start))
		}(i)
	}
}
